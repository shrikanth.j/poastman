/**
 * We can use this index file to export all the APIs inside common folder.
 * Common folder will have all APIs that are commonly used for browser and desktop app.
 */

// @TO DO - Use single isElectron util for preload and webpack utils
const isElectron = () => {
  // To determine if we are running the electron app, we first check if we have the process object
  // and process.versions.electron is present.
  // This should cover the case for both the main and the renderer process.
  // Only case when this won't tell us reliably that we are running the electron app is the case
  // when node-integration is disabled. In this case the process object is not defined
  if (typeof process !== 'undefined' && typeof process.versions === 'object' && !!process.versions.electron) {
      return true;
  }

  // Use userAgent to determine whether an Electron app
  // This is the case when the previous check was inconclusive (case when node integration might have been disabled)
  if (typeof navigator === 'object' && typeof navigator.userAgent === 'string' && navigator.userAgent.toLowerCase().indexOf('electron') >= 0) {
      return true;
  }

  return false;
};

window = window || {};

module.exports = {
  isElectron
};
