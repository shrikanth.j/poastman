(window.webpackJsonp=window.webpackJsonp||[]).push([[167],{"./version-control/common/TabEmptyState.js":function(e,t,o){"use strict";o.r(t),o.d(t,"default",(function(){return l}));var n=o("../../node_modules/react/index.js"),r=o.n(n),a=o("../../node_modules/@postman/aether/esmLib/index.js");const s=o("../../node_modules/styled-components/dist/styled-components.browser.esm.js").default.div`
  display: flex;
  flex-direction: column;
  align-items: center;

  .empty-state__title {
    margin-top: ${({theme:e})=>e["spacing-l"]};
  }

  .empty-state__description {
    margin-top: ${({theme:e})=>e["spacing-xs"]};
    margin-bottom: ${({theme:e})=>e["spacing-l"]};
  }
`;function l(e){return r.a.createElement(s,{className:e.className},e.illustration,r.a.createElement(a.Heading,{type:"h4",text:e.title,hasBottomSpacing:!0,className:"empty-state__title"}),r.a.createElement(a.Text,{type:"body-medium",color:"content-color-secondary",className:"empty-state__description"},e.message),e.showAction&&e.action)}},"./version-control/common/context-bar-items.js":function(e,t,o){"use strict";o.r(t),o.d(t,"ContextBarDescription",(function(){return r})),o.d(t,"ContextBarListItem",(function(){return a})),o.d(t,"ContextBarSubtext",(function(){return s})),o.d(t,"ContextBarContainer",(function(){return l})),o.d(t,"ContextBarLoading",(function(){return i})),o.d(t,"ContextBarEmptyStateContainer",(function(){return c}));var n=o("../../node_modules/styled-components/dist/styled-components.browser.esm.js");const r=n.default.span`
  font-size: var(--text-size-m);
  color: var(--content-color-secondary);
  font-weight: var(--text-weight-regular);
  margin-left: var(--spacing-s);
  display: flex;
`,a=n.default.span`
  color: var(--content-color-primary);
  font-size: var(--text-size-m);

  &:hover {
    text-decoration: underline;
    cursor: pointer;
  }
`,s=n.default.p`
  margin-top: 0;
  margin-bottom: 0;
  color: var(--content-color-tertiary);
  font-size: var(--text-size-s);
`,l=n.default.div`
  margin-left: var(--spacing-s);
  margin-top: var(--spacing-l);
  padding-bottom: var(--spacing-xl);

  div {
    margin-bottom: var(--spacing-s);
  }
`,i=n.default.div`
  position: absolute;
  top: 50%;
  left: 50%;
`,c=n.default.div`
  position: absolute;
  top: 30%;
  left: 15%;
  min-width: 300px;

  p {
    font-size: var(--text-size-m);
    color: var(--content-color-secondary);
    margin: 0;
  }

  .context-bar-empty-state--full-width {
    width: 100%;
  }
`},"./version-control/fork/ForkListBody.js":function(e,t,o){"use strict";o.r(t),function(e){o.d(t,"default",(function(){return w}));var n=o("../../node_modules/react/index.js"),r=o.n(n),a=o("../../node_modules/styled-components/dist/styled-components.browser.esm.js"),s=o("../../node_modules/@postman/aether/esmLib/index.js"),l=o("../../node_modules/@postman/aether-icons/esmLib/index.js"),i=o("./node_modules/moment-timezone/index.js"),c=o.n(i),d=o("./version-control/common/TabLoader.js"),m=o("./version-control/common/index.js"),u=o("./js/components/base/Avatar.js"),f=o("./version-control/common/TabEmptyState.js"),p=o("./version-control/common/context-bar-items.js"),g=(o("./js/services/NavigationService.js"),o("./js/external-navigation/ExternalNavigationService.js")),k=o("./appsdk/components/link/Link.js"),v=o("./js/components/base/Buttons.js"),y=o("./js/utils/PluralizeHelper.js"),x=o("./version-control/common/ItemLink.js");const b=a.default.div`
  display: inline-block;
`,h=a.default.div`
  float: left;
  width: 100%;
`,j=Object(a.default)(m.CustomTooltip)`
  float: right;
  margin-top: var(--spacing-s);
  margin-right: var(--spacing-xxl);
`,E=a.default.div`
  font-size: var(--text-size-m);
  color: var(--content-color-primary);
  font-weight: var(--text-weight-medium);
  margin-bottom: var(--spacing-m);
`,C=a.default.span`
  color: var(--content-color-info);
  font-size: var(--text-size-m);
  margin-top: var(--spacing-l);
  cursor: pointer;
  padding-bottom: var(--spacing-xl);

  &:hover {
    text-decoration: underline
  }

  &.forks-external-link {
    color: var(--content-color-secondary);
    padding-bottom: 0px;

    :hover {
      text-decoration: none;
      color: var(--content-color-link);
    }
  }
`;Object(a.default)(l.Icon)`
  position: relative;
  top: 2px;
`;function w(t){const o={collection:"https://go.pstmn.io/docs-collection-forking",environment:"https://go.pstmn.io/docs-environment-forking"},a=r.a.createElement(C,{className:"forks-external-link",onClick:()=>Object(g.openExternalLink)(o[t.modelName],"_blank")},r.a.createElement(s.Text,{type:"link-default"},"forks")),l=r.a.createElement(s.Text,{type:"para"},"They aren't visible because they're in workspaces you don't have access to. Learn more about ",a,"."),i=r.a.createElement(s.Text,{type:"para"},"All forks created from this ",t.modelName," will appear here. Learn more about ",a,"."),m=r.a.createElement(v.Button,{type:"primary",size:"small",onClick:t.fetchForks},"Retry"),w=y.default.pluralize({count:e.get(t,"forkInfo.totalForks"),singular:"fork",plural:"forks"});if(t.loading)return r.a.createElement(d.default,null);if(t.isOffline)return r.a.createElement(p.ContextBarEmptyStateContainer,null,r.a.createElement(f.default,{showAction:!0,illustration:r.a.createElement(s.IllustrationCheckInternetConnection,null),title:"Check your connection",message:"Get online to view your list of forks.",action:m}));if(t.error){const o="object"==typeof t.error&&"forbiddenError"===t.error.name;return r.a.createElement(p.ContextBarEmptyStateContainer,null,r.a.createElement(f.default,{illustration:o?r.a.createElement(s.IllustrationNoPermission,null):r.a.createElement(s.IllustrationInternalServerError,null),title:o?"Cannot view forks":"Unable to load list of forks",message:o?`You don't have permission to view the forks of this ${t.modelName}`:e.get(t,"error.error.message")||"Try refetching forks"}))}if(t.isEmpty){const o=Boolean(e.get(t,"forkInfo.hiddenForks"));return r.a.createElement(p.ContextBarEmptyStateContainer,null,r.a.createElement(s.BlankState,{className:"context-bar-empty-state--full-width",title:o?`${e.get(t,"forkInfo.totalForks")} ${w} of ${e.get(t,"name")}`:"No forks created yet",description:o?l:i},r.a.createElement(s.IllustrationNoFork,null)))}return r.a.createElement(n.Fragment,null,r.a.createElement(p.ContextBarDescription,null,e.get(t,"forkInfo.totalForks")," ",w," of ",e.get(t,"name"),".",e.get(t,"forkInfo.hiddenForks")>0?` ${e.get(t,"forkInfo.hiddenForks")} of those aren't in this list because they're in workspaces you don't have access to.`:""),r.a.createElement(p.ContextBarContainer,null,r.a.createElement(E,null,"Recently created:"),(t.forks||[]).map((o=>r.a.createElement(h,{key:o.modelId},r.a.createElement(b,null,r.a.createElement(k.default,{to:o.modelId?`${pm.artemisUrl}/${t.modelName}/${o.modelId}`:"",disabled:!o.modelId},r.a.createElement(p.ContextBarListItem,null,r.a.createElement(x.default,{text:o.forkName}))),r.a.createElement(p.ContextBarSubtext,null,`Created on: ${c()(o.createdAt).format("DD MMM, YYYY")}`)),r.a.createElement(j,{align:"bottom",body:e.get(o,"createdBy.name")},r.a.createElement(u.default,{size:"medium",userId:e.get(o,"createdBy.id"),customPic:e.get(o,"createdBy.profilePicUrl")}))))),r.a.createElement(k.default,{to:{routeIdentifier:"build.forks",routeParams:{model:t.modelName,id:e.get(t,"modelUID")}}},r.a.createElement(s.Text,{type:"link-primary"},e.get(t,"forkInfo.totalForks")>(t.forks||[]).length?"View all forks":"View details"))))}}.call(this,o("./node_modules/lodash/lodash.js"))},"./version-control/fork/ForkListing.js":function(e,t,o){"use strict";o.r(t),function(e){o.d(t,"default",(function(){return v}));var n=o("../../node_modules/react/index.js"),r=o.n(n),a=o("../../node_modules/styled-components/dist/styled-components.browser.esm.js"),s=o("./node_modules/querystring-browser/querystring.js"),l=o.n(s),i=o("./js/stores/SyncStatusStore.js"),c=o("./js/stores/StoreManager.js"),d=o("./appsdk/contextbar/ContextBarViewHeader.js"),m=o("./version-control/fork/ForkListBody.js"),u=o("./js/modules/services/RemoteSyncRequestService.js");const f=a.default.div`
  overflow-y: auto;
`,p=12,g="createdAt",k="desc";function v(t){const[o,a]=Object(n.useState)([]),[s,v]=Object(n.useState)({}),[y,x]=Object(n.useState)(!0),[b,h]=Object(n.useState)(!1),[j,E]=Object(n.useState)(null),C=Object(c.resolveStoreInstance)(i.default),w=!Object(c.resolveStoreInstance)(i.default).isSocketConnected,S={populateUsers:!0,pageSize:p,sortBy:g,sortType:k},I=()=>{x(!0),E(null),C.onSyncAvailable({timeout:5e3}).then((()=>u.default.request(`/${t.contextData.model}/${t.contextData.uid}/fork-list?${l.a.stringify(S)}`))).then((t=>{a(e.get(t,"body.data.forks")),v({publicForks:e.get(t,"body.data.publicForkCount"),privateForks:e.get(t,"body.data.privateForkCount"),hiddenForks:e.get(t,"body.data.hiddenForkCount"),totalForks:e.get(t,"body.data.totalForkCount")}),h(!(e.get(t,"body.data.forks")||[]).length),x(!1)})).catch((e=>{E(e),x(!1),pm.logger.error("Unable to fetch fork list",e)}))};return Object(n.useEffect)((()=>{I()}),[]),r.a.createElement(f,null,r.a.createElement(d.ContextBarViewHeader,{title:"Forks",onClose:t.onClose}),r.a.createElement(m.default,{loading:y,isOffline:w,isEmpty:b,error:j,forks:o,fetchForks:I,forkInfo:s,modelName:t.contextData.model,name:t.contextData.name,modelUID:t.contextData.uid}))}}.call(this,o("./node_modules/lodash/lodash.js"))}}]);