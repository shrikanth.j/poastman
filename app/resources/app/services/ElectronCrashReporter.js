var _ = require('lodash'),
  electron = require('electron'),
  crashReporter = electron.crashReporter,
  releaseChannel = _.lowerCase(require('./AppConfigService').getConfig('__WP_RELEASE_CHANNEL__'));

class ElectronCrashReporter {
  init () {
    let sentryURL;
    if (releaseChannel === 'prod') {
      sentryURL = 'https://o1224273.ingest.sentry.io/api/6543787/minidump/?sentry_key=4657359d34004de980b15867cd04eb7a';
    }
    else {
      sentryURL = 'https://o1224273.ingest.sentry.io/api/6567626/minidump/?sentry_key=474eeea76b4d438496a9c1f839dc1711';
    }

    // Initialize the crash reporter from electron to collect crash dumps for crashes happening in main & renderer process.
    // Crash dumps will be present inside user data directory.

    crashReporter.start({
      uploadToServer: true,
      submitURL: sentryURL,
      compress: true
    });
  }
}

exports.ElectronCrashReporter = new ElectronCrashReporter();
