(window.webpackJsonp=window.webpackJsonp||[]).push([[126],{"./documentation/components/DocumentationIntersectionObserver/index.js":function(e,t,n){"use strict";n.r(t);var o=n("./documentation/components/DocumentationIntersectionObserver/DocumentationIntersectionObserver.js");n.d(t,"IntersectionObserverContext",(function(){return o.IntersectionObserverContext})),n.d(t,"useIntersectionObserver",(function(){return o.useIntersectionObserver})),t.default=o.default},"./runtime-repl/extensible-collection/workbench/ECFolder/FolderEditor.jsx":function(e,t,n){"use strict";n.r(t),function(e){var o=n("../../node_modules/react/index.js"),a=n.n(o),r=n("../../node_modules/mobx-react/dist/mobx-react.module.js"),i=n("../../node_modules/@postman/aether/esmLib/index.js"),s=n("./runtime-repl/request-grpc/components/TabbedPane.jsx"),l=n("./runtime-repl/_common/_api/RuntimePreferenceInterface.js"),c=n("./runtime-repl/_common/RuntimePreferenceConstants.js"),d=n("./runtime-repl/extensible-collection/index.ts"),u=n("./runtime-repl/extensible-collection/configurations/Folder.ts"),m=n("./runtime-repl/extensible-collection/workbench/Overview.jsx"),p=n("./runtime-repl/extensible-collection/api/ExtensibleCollectionItemAPI.ts");const b=Object(r.observer)((({state:t,update:n})=>{const{addToast:o}=Object(i.useToasts)(),r=e.get(p.default.cacheAPI.get(t.id),"extensions.documentation.content",""),b=e.get(l.default.getObservableStore().find(c.BETA_COLLECTION_BANNER_IN_TAB),"value",!0);return a.a.createElement(a.a.Fragment,null,b&&a.a.createElement(i.Banner,{status:"info",type:"global",onDismiss:()=>l.default.set(c.BETA_COLLECTION_BANNER_IN_TAB,!1)},"Folders with Websocket and gRPC requests have limited functionality for now. You’ll be able to do more soon."),a.a.createElement(s.TabbedPane,{isPane:!1},a.a.createElement(s.PaneTab,{id:"OVERVIEW",label:"Overview"},a.a.createElement(m.default,{entityType:"folder",info:{createdAt:t.createdAt},hasPermission:u.default.hasPermission(t.id,d.ItemAction.SAVE),savedDescription:r,description:e.get(t.extensions,"documentation.content",""),onDescriptionChange:e=>n.path("extensions.documentation.content",e),onDescriptionReset:()=>n.path("extensions.documentation.content",r),onDescriptionSave:(n,a)=>{u.default.get(t.id).then((({extensions:o})=>u.default.update({id:t.id,extensions:e.merge(o,{documentation:{content:n}})}))).then((()=>a(null))).catch((e=>{a(e),pm.logger.error("FolderEditor~onDescriptionSave",e),o({status:"error",description:"Something went wrong while updating the folder description."})}))}})),a.a.createElement(s.PaneTab,{id:"TESTS",label:"Tests"},a.a.createElement(i.BlankState,{title:"Coming soon",description:"You’ll be able to add tests to this folder soon."},a.a.createElement(i.IllustrationTestPass,null)))))}));t.default=b}.call(this,n("./node_modules/lodash/lodash.js"))},"./runtime-repl/extensible-collection/workbench/Overview.jsx":function(e,t,n){"use strict";n.r(t),function(e){var o=n("../../node_modules/styled-components/dist/styled-components.browser.esm.js"),a=n("../../node_modules/react/index.js"),r=n.n(a),i=n("../../node_modules/@postman/aether/esmLib/index.js"),s=n("../../packages/documentation/documentation-core/index.js"),l=n("./js/stores/get-store.js"),c=n("./runtime-repl/_common/components/EntityMetaInfoView/EntityMetaInfoView.js"),d=n("./runtime-repl/_common/DisabledTooltipConstants.js"),u=n("./documentation/components/DocumentationIntersectionObserver/index.js"),m=n("./runtime-repl/extensible-collection/workbench/Summary.tsx");const p=Object(o.default)(i.Flex)`
  overflow-y: auto;
  padding-top: var(--spacing-l);
`,b=Object(o.default)(i.ResponsiveContainer)`
  width: 100%;
`,f=Object(o.default)(i.ResponsiveContainer)`
  font-size: var(--text-size-m);
  line-height: var(--line-height-m);

  ${e=>e.inEditMode&&o.css`
    height: 100%;
    overflow: unset;
  `}

  .description-editor-wrapper {
    height: 100%;
    overflow: hidden;

    .description-editor__markdown-editor {
      pre {
        word-break: break-word;
      }
    }
  }

  .description-preview {
    overflow: auto;

    &__edit-icon {
      margin-left: var(--spacing-l);
    }

    &__markdown {
      word-break: break-word;
    }
  }
`,h=Object(a.forwardRef)((({entityType:t,info:n,description:o,savedDescription:h,onDescriptionSave:g,onDescriptionReset:v,onDescriptionChange:E=e.noop,summary:y,enableSummary:T=!1,onSummarySave:x=e.noop,hasPermission:w=!0},j)=>{const[O,_]=Object(a.useState)(!1),C=Object(l.getStore)("OnlineStatusStore").userCanSave,S=()=>{_(!1),v()},I=e=>{g(e,(e=>{e||_(!1)}))};return Object(a.useImperativeHandle)(j,(()=>({setEditMode:_}))),r.a.createElement(u.default,null,(({rootRef:e})=>r.a.createElement(p,{grow:1,shrink:1,justifyContent:"center",ref:e},r.a.createElement(b,{type:"row",maxWidth:"container-width-medium",overflow:"unset"},r.a.createElement(i.ResponsiveContainer,{type:"column",span:1,hiddenFor:["mobile"]}),r.a.createElement(f,{type:"column",span:7,mobile:12,minHeight:"100%",inEditMode:O&&C},O&&C?r.a.createElement(s.DescriptionEditor,{entityType:t,description:h,onChange:E,onCancel:S,onSave:I,placeholder:`Make things easier for your teammates with a complete ${t} description.`}):r.a.createElement(s.DescriptionPreview,{entityType:t,description:o,onEditButtonClick:()=>_(!0),editable:C,canEdit:w,showEmptyPlaceholder:!0,tooltip:C?w?null:d.DISABLED_TOOLTIP_NO_PERMISSION:d.DISABLED_TOOLTIP_IS_OFFLINE})),r.a.createElement(i.ResponsiveContainer,{type:"column",span:3,mobile:12,padding:{paddingRight:"spacing-s",paddingLeft:"spacing-s"}},r.a.createElement(i.Flex,{gap:"spacing-l",direction:"column"},T&&r.a.createElement(m.default,{summary:y,placeholder:`Briefly explain this ${t} with a summary.`,isEditable:C&&w,onSubmit:x}),r.a.createElement(c.default,{userFriendlyEntityName:t,info:{createdAt:n.createdAt,owner:n.createdBy}}))),r.a.createElement(i.ResponsiveContainer,{type:"column",span:1,hiddenFor:["mobile"]})))))}));t.default=h}.call(this,n("./node_modules/lodash/lodash.js"))},"./runtime-repl/extensible-collection/workbench/Summary.tsx":function(e,t,n){"use strict";n.r(t);var o=n("../../node_modules/styled-components/dist/styled-components.browser.esm.js"),a=n("../../node_modules/react/index.js"),r=n.n(a),i=n("../../node_modules/@postman/aether/esmLib/index.js"),s=n("./js/components/base/keymaps/KeyMaps.js");const l=Object(o.default)(i.Text)`
  padding: var(--spacing-xs) var(--spacing-s);
  white-space: pre-wrap
`,c=o.default.div`
  display: flex;
  align-items: center;
  flex: 1 auto;
  min-height: var(--size-m);
  margin-left: -8px;
  cursor: ${e=>e.isEditable?"text":"default"};

  ${e=>e.isEditable&&o.css`
    &:hover {
      border-radius: var(--border-radius-default);
      background-color: var(--background-color-tertiary);
    }
  `}
`;t.default=({summary:e,onSubmit:t,onChange:n=(()=>{}),placeholder:o="Add a brief summary.",characterLimit:d=140,isEditable:u=!0,showLabel:m=!0})=>{const[p,b]=Object(a.useState)(!1),[f,h]=Object(a.useState)(!1),[g,v]=Object(a.useState)(""),[E,y]=Object(a.useState)(e);Object(a.useEffect)((()=>{!p&&y(e)}),[e]);const T=()=>{u&&!f&&(h(!1),b(!1),t(E,(e=>{e&&b(!0)})))};return r.a.createElement(a.Fragment,null,p?r.a.createElement(s.default,{handlers:{enter:T,submit:T,quit:()=>{b(!1),h(!1),y(e)}}},r.a.createElement("div",null,r.a.createElement(i.TextArea,{label:r.a.createElement(i.Label,{text:m?"Summary":""}),value:E,placeholder:o,maxLength:d,validationStatus:f?"error":"",validationMessage:g,onChange:(e,{charLimit:t})=>{var o,a;if(!u)return;const r=null===(o=null==e?void 0:e.nativeEvent)||void 0===o?void 0:o.inputType;let i=null===(a=null==e?void 0:e.target)||void 0===a?void 0:a.value;var s;"insertLineBreak"!==r&&("insertFromPaste"===r&&(i=(s=i)?s.replace(/\r\n|\r|\n/g," "):""),i.length>d?(h(!0),v(`You are over permitted characters limit by ${t} characters`)):f&&(h(!1),v("")),y(i),n(i))},onBlur:T,isResizable:!1,autoFocus:!0}))):r.a.createElement(a.Fragment,null,m&&r.a.createElement(i.Heading,{type:"h6",text:"Summary",color:"content-color-secondary",hasBottomSpacing:!0}),r.a.createElement(c,{isEditable:u,onClick:()=>u&&b(!0)},r.a.createElement(l,{type:"para",maxLines:10,color:E?"content-color-primary":"content-color-tertiary"},E||(u?o:"Summary is yet to be added.")))))}},"./runtime-repl/request-grpc/components/TabbedPane.jsx":function(e,t,n){"use strict";n.r(t),function(e){n.d(t,"TabbedPane",(function(){return x})),n.d(t,"PaneTab",(function(){return j}));var o=n("../../node_modules/react/index.js"),a=n.n(o),r=n("../../node_modules/styled-components/dist/styled-components.browser.esm.js"),i=n("./js/components/base/Panes.js"),s=n("./runtime-repl/_common/ListTypeConstants.js"),l=n("./runtime-repl/_common/components/EntityEditorTabs.js"),c=n("./js/constants/Panes.js"),d=n("./runtime-repl/extensible-collection/index.ts"),u=n("./runtime-repl/request-http/RequesterTabLayoutConstants.js"),m=n("../../node_modules/@postman/aether/esmLib/index.js"),p=n("../../node_modules/@postman/aether-icons/esmLib/index.js");function b(){return b=Object.assign||function(e){for(var t=1;t<arguments.length;t++){var n=arguments[t];for(var o in n)Object.prototype.hasOwnProperty.call(n,o)&&(e[o]=n[o])}return e},b.apply(this,arguments)}const f=r.default.div`
  display: flex;
  flex-direction: column;
  height: 100%;
  overflow: auto;
`,h=r.default.div`
  display: inline-flex;
  width: 8px;
  height: 8px;
  border-radius: 8px;
  margin-left: var(--spacing-xs);
  background-color: ${({hasError:e})=>e?" var(--base-color-error)":" var(--base-color-success)"};
`,g=r.default.span`
  margin-left: var(--spacing-xs);
  color: var(--content-color-success);
`,v=r.default.div`
  display: flex;
  align-items: center;
  gap: 0 var(--spacing-l);
  padding: 0 var(--spacing-l);
  font-size: var(--text-size-s);
  line-height: var(--line-height-m);
  color: var(--content-color-secondary);
`,E=r.default.div`
  display: flex;
  align-items: center;
  flex: 1;
`,y=r.default.div`
  display: flex;
  flex-direction: column;
  flex: 1;
  overflow-y: auto;

  ${e=>!e.isActive&&r.css`
    display: none;
  `}

  & > * {
    flex: 1;
  }
`,T=Object(r.default)(m.Button)`
  margin-right: var(--spacing-s);
`,x=a.a.forwardRef((({headerComponent:t,additionalControls:n,verticalComponent:o,isPane:r=!0,invertToggle:s=!1,tabListThreshold:l,children:m,...p},h)=>{const g=a.a.Children.toArray(m).filter((e=>a.a.isValidElement(e)&&e.type===j&&"string"==typeof e.props.id&&e.props.id.length)),[v,E]=a.a.useState(e.get(g[0],"props.id")),y=a.a.useRef(),T=Object(d.useTabLayout)(),x=r?i.Pane:f;function O(e){E(e),y.current&&y.current.unCollapse("y")}return a.a.createElement(x,b({ref:function(e){e&&(e.getActiveTab=()=>v,e.setActiveTab=O),y.current=r?e:null,h&&h(e)}},p),a.a.createElement(i.PaneHeader,{defaultLayout:c.PANE_LAYOUT_HORIZONTAL,horizontalComponent:a.a.createElement(w,{headerComponent:t,activeTabId:v,onTabSelect:O,tabs:g,additionalControls:n,paneRef:y,tabListThreshold:l,invertToggle:s,collapseToggleHandler:()=>{var e;null===(e=y.current)||void 0===e||e.handleCollapseToggle(T===u.REQUESTER_TAB_LAYOUT_1_COLUMN?"y":"x")},showToggle:r}),verticalComponent:o}),a.a.createElement(i.PaneContent,null,p.additionalContent,g.map((e=>a.a.cloneElement(e,{key:e.props.id,isActive:e.props.id===v})))))}));function w({isCollapsed:e,headerComponent:t,activeTabId:n,onTabSelect:o,tabs:r,additionalControls:i,paneRef:c,tabListThreshold:m,invertToggle:b,collapseToggleHandler:f,showToggle:y}){var x;const w=Object(d.useTabLayout)(),j=(null===(x=c.current)||void 0===x?void 0:x.state.width)<m?s.DROPDOWN:s.HORIZONTAL_LIST;let O=w===u.REQUESTER_TAB_LAYOUT_1_COLUMN?e?p.IconDirectionUp:p.IconDirectionDown:p.IconDirectionRight;return b&&(O=w===u.REQUESTER_TAB_LAYOUT_1_COLUMN?e?p.IconDirectionDown:p.IconDirectionUp:p.IconDirectionLeft),a.a.createElement(a.a.Fragment,null,t,a.a.createElement(E,null,a.a.createElement(l.default,{activeTab:n,onTabSelect:o,tabsList:r.map((e=>({refKey:e.props.id,label:e.props.label||e.props.id,getBadge:()=>{const{badge:t,hasError:n}=e.props;return!0===t?a.a.createElement(h,{hasError:n}):Number.isInteger(t)?a.a.createElement(g,null,"(",t,")"):a.a.isValidElement(t)?t:null}}))),tabsListType:j,isCollapsed:e}),a.a.createElement(v,null,i),y&&a.a.createElement(T,{type:"muted-plain",icon:a.a.createElement(O,{color:"content-color-tertiary"}),onClick:f})))}function j({id:e,label:t,badge:n,hasError:o=!1,isActive:r,children:i}){if("string"!=typeof e||!e.length)throw new TypeError('PaneTab must have a string "id" prop');if(null!=t&&"string"!=typeof t)throw new TypeError('PaneTab "label" prop must be a string, if provided');if(null!=n&&"boolean"!=typeof n&&!Number.isInteger(n)&&!a.a.isValidElement(n))throw new TypeError('PaneTab "badge" prop must be a boolean, integer or a valid component, if provided');if("boolean"!=typeof o)throw new TypeError('PaneTab "hasError" prop must be a boolean, if provided');return a.a.createElement(y,{isActive:r},i)}}.call(this,n("./node_modules/lodash/lodash.js"))}}]);