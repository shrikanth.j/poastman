const authDataInterface = require('./authDataInterface'),
  BrowserWindow = require('electron').BrowserWindow,
  sendAnalyticsEvent = require('./sendAnalytics');

/**
 *
 */
async function performBackgroundShellMigration () {
  try {
    if (await isShellMigrationDone()) {
      pm.logger.info('performBackgroundShellMigration: Migration has already been done. Bailing out');
      return;
    }

    pm.logger.info('performBackgroundShellMigration: Starting shell data migration');

    const shellData = await getDataFromShell();

    if (!shellData) {
      pm.logger.info('performBackgroundShellMigration: Got no data from shell. Bailing out');
      return;
    }

    pm.logger.info('performBackgroundShellMigration: Got data from shell. Writing to storage');

    await authDataInterface.setData(shellData);

    sendAnalyticsEvent('shell-migration', 'successful');

    pm.logger.info('performBackgroundShellMigration: Successfully completed migration');
  }
  catch (e) {
    pm.logger.warn('performBackgroundShellMigration: Error while performing migration', e);

    sendAnalyticsEvent('shell-migration', 'failed', `${e && e.name}:${e && e.message}`);
  }
}

/**
 *
 */
function getDataFromShell () {
  // Maintain an array of known keys which will be present in the app
  const SHELL_KEYS = new Set(['v8PartitionsNamespaceMeta', 'partitions', 'v8Partitions', 'users']);

  return new Promise((resolve, reject) => {
    pm.sdk.IPC.subscribe('shellData', (event, arg = {}) => {
      Object.keys(arg).forEach((key) => {
          try {
            arg[key] = JSON.parse(arg[key]);
          }
          catch (e) {
            sendAnalyticsEvent('get-data-from-shell', 'failed', `${e && e.name}:${key}:${e && e.message}`);
            return SHELL_KEYS.has(key) && reject(e);
          }
      });

      sendAnalyticsEvent('get-data-from-shell', 'successful');
      resolve(arg);
    });

    let win = BrowserWindow.getFocusedWindow();
    win = win ? win : BrowserWindow.getAllWindows().find((win) => (/shell.html/).test(win.webContents.getURL()));

    // If the window is not in focus, we cannot do the migration, so we reject the promise
    if (!win) {
      return reject(new Error('getDataFromShell:Window not in focus'));
    }

    win.webContents.send('shellMessage', { type: 'getShellData' });
  });
}

/**
 *
 */
function isShellMigrationDone () {
  return authDataInterface.exists();
}

module.exports = {
  performBackgroundShellMigration,
  isShellMigrationDone
};
