(window.webpackJsonp=window.webpackJsonp||[]).push([[172],{"./version-control/pull-request/components/PullRequestMeta/index.js":function(e,t,s){"use strict";s.r(t),s.d(t,"default",(function(){return E}));var n=s("../../node_modules/react/index.js"),a=s.n(n),r=s("./node_modules/lodash/lodash.js"),l=s.n(r),o=s("../../node_modules/styled-components/dist/styled-components.browser.esm.js"),i=s("../../node_modules/@postman/aether/esmLib/index.js"),c=s("./version-control/pull-request/components/PullRequestMeta/infoComponents.js"),u=s("./appsdk/contextbar/ContextBarViewHeader.js"),d=s("./version-control/constants.js");const m={merged:"Merged",declined:"Declined"},p=o.default.div`
  /** increasing specificity */
  .pull-request-meta__header.pull-request-meta__header {
    padding-bottom: ${({theme:e})=>e["spacing-l"]};
  }
`;class E extends n.Component{constructor(e){super(e)}get isActivePR(){return[d.PR_STATES.OPEN,d.PR_STATES.APPROVED].includes(l.a.get(this.props.contextData,"pullRequest.status"))}pullRequestReviewerStatusBadge(e){return e===d.PR_STATES.APPROVED?a.a.createElement(i.Badge,{text:d.PR_STATES.APPROVED,status:"success"}):e===d.PR_STATES.MERGED?a.a.createElement(i.Badge,{text:d.PR_STATES.MERGED,status:"success"}):e===d.PR_STATES.DECLINED?a.a.createElement(i.Badge,{text:d.PR_STATES.DECLINED,status:"critical"}):void 0}render(){return a.a.createElement(p,null,a.a.createElement(u.ContextBarViewHeader,{title:"Pull request details",onClose:this.props.onClose,className:"pull-request-meta__header"}),a.a.createElement("div",{className:"pull-request-meta"},!this.isActivePR&&a.a.createElement(c.Info,{title:`${m[l.a.get(this.props.contextData,"pullRequest.status")]} by`},a.a.createElement(c.User,{user:l.a.get(this.props.contextData,"pullRequest.updatedBy")})),a.a.createElement(c.Info,{title:"Created by"},a.a.createElement(c.User,{user:l.a.get(this.props.contextData,"pullRequest.createdBy")})),a.a.createElement(c.Info,{title:"Created on"},a.a.createElement(c.Date,{date:l.a.get(this.props.contextData,"pullRequest.createdAt"),format:"DD MMM YYYY"})),this.isActivePR&&a.a.createElement(c.Info,{title:"Last updated"},a.a.createElement(c.Date,{date:l.a.get(this.props.contextData,"pullRequest.updatedAt"),relative:!0})),a.a.createElement(c.Info,{title:"Reviewers"},l.a.get(this.props.contextData,"pullRequest.reviewers.length",0)?a.a.createElement("ul",{className:"pull-request-reviewers-list"},l()(this.props.contextData).get("pullRequest.reviewers",[]).map((e=>a.a.createElement(c.User,{key:e.id,user:e,renderRight:()=>this.pullRequestReviewerStatusBadge(e.status)})))):a.a.createElement(i.Text,{type:"body-medium",color:"content-color-primary"},"No reviewers"))))}}},"./version-control/pull-request/components/PullRequestMeta/infoComponents.js":function(e,t,s){"use strict";s.r(t),s.d(t,"Info",(function(){return m})),s.d(t,"Date",(function(){return p})),s.d(t,"User",(function(){return f}));var n=s("../../node_modules/react/index.js"),a=s.n(n),r=s("../../node_modules/@postman/aether/esmLib/index.js"),l=s("./node_modules/moment-timezone/index.js"),o=s.n(l),i=s("../../node_modules/styled-components/dist/styled-components.browser.esm.js"),c=s("./js/components/base/Avatar.js"),u=s("./version-control/common/index.js");const d=i.default.div`
  & + & {
    margin-top: ${({theme:e})=>e["spacing-xl"]};
  }

  .pr-meta-info__title {
    margin-bottom: ${({theme:e})=>e["spacing-s"]};
  }

  .pr-meta-info__content {
    display: flex;
  }
`;function m(e){const{title:t,children:s}=e;return a.a.createElement(d,null,a.a.createElement(r.Heading,{type:"h6",text:t,color:"content-color-secondary",className:"pr-meta-info__title"}),a.a.createElement(r.Flex,null,s))}function p(e){const{date:t,format:s,relative:n}=e;let l;return n?l=o()(t).fromNow():s&&(l=o()(t).format(s)),a.a.createElement(r.Text,{type:"body-medium",color:"content-color-primary"},l)}const E=i.default.li`
  display: flex;
  width: 100%;

  .space-filler {
    flex: auto;
  }
`;function f(e){const{user:t,renderRight:s}=e;return a.a.createElement(E,null,a.a.createElement(c.default,{size:"medium",userId:t.id,customPic:t.profilePicUrl}),a.a.createElement(u.Username,{className:"pull-request-user__name",currentUserId:t.id,id:t.id,user:t}),a.a.createElement("span",{className:"space-filler"}),s&&s())}}}]);