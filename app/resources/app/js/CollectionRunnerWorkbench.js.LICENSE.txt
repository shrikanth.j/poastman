/*!
 * https://github.com/Starcounter-Jack/JSON-Patch
 * (c) 2017 Joachim Wester
 * MIT license
 */

/*!
 * https://github.com/Starcounter-Jack/JSON-Patch
 * (c) 2017-2022 Joachim Wester
 * MIT licensed
 */

//! Copyright (c) JS Foundation and other contributors

//! github.com/moment/moment-timezone

//! license : MIT

//! moment-timezone.js

//! moment.js

//! version : 0.5.14
