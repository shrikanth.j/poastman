const REACT_DEVELOPER_TOOLS = {
  name: 'React Devtools',
  id: 'fmkadmapgofadopljbjfkapdkoienihi'
};

const MOBX_DEVTOOLS = {
  name: 'Mobx Devtools',
  id: 'pfgnfdagidkfgccljigdamigbcnndkod'
};

const ListOfDevtools = [
  REACT_DEVELOPER_TOOLS,
  MOBX_DEVTOOLS
];

module.exports = {
  ListOfDevtools
};
