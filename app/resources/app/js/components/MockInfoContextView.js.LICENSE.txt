/*!
  * chartjs-adapter-moment v1.0.0
  * https://www.chartjs.org
  * (c) 2021 chartjs-adapter-moment Contributors
  * Released under the MIT license
  */

/*!
 * Chart.js v3.3.0
 * https://www.chartjs.org
 * (c) 2021 Chart.js Contributors
 * Released under the MIT License
 */

/*!
 * chartjs-plugin-trendline.js
 * Version: 1.0.2
 *
 * Copyright 2022 Marcus Alsterfjord
 * Released under the MIT license
 * https://github.com/Makanz/chartjs-plugin-trendline/blob/master/README.md
 *
 * Mod by: vesal: accept also xy-data so works with scatter
 */
